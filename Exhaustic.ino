// ---------- CONSTANTS ------------ //
const int LDR_THRESHOLD = 400;
const int DEBUG = 0;

// ---------- PIN MAPPINGS ------------ //
const int PWMPinA = 3;                      //Motor A PWM pin identified
const int DirPinA = 12;                       //Motor A Direction pin identified
const int ledPin = 13;
const int ldrSensorPin = 3;
const int tempSensorPin = 2;

// ---------- GLOBALS ------------ //
unsigned long prevTime;
double setPoint;
double e0, e1;
double Kp, Ki, Kd;
double P, I, D;
int ldrSensorValue = 0;
int tempSensorValue = 0;
int onOffOutput = 0;
int unboundedOutput = 0;
int boundedOutput = 0;
int scaledOutput = 0;
int usePID = 1;
int firstReading = 0;
int secondReading = 0;
int thirdReading = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(DirPinA, OUTPUT);       // Motor A Direction pin set as an output
  pinMode(PWMPinA, OUTPUT);       // Motor A speed pin set as an output
  pinMode(ledPin, OUTPUT);       // Use onboard LED to indicate system operation status
  digitalWrite(DirPinA, HIGH);     //Set the direction pin to HIGH for forward rotation

  setPoint = 970;
  Kp = 20;
  Ki = 0.01;
  Kd = 0;


}

void loop() {
  // put your main code here, to run repeatedly:

  getNewSerialData();

  firstReading = analogRead(tempSensorPin);      //Read temp and light sensor values
  ldrSensorValue = analogRead(ldrSensorPin);
  //ldrSensorValue = 400;

  
  tempSensorValue = (int)(firstReading + secondReading + thirdReading) / 3;
  thirdReading = secondReading;
  secondReading = firstReading;
  

  unboundedOutput = (int)PID(tempSensorValue);
  boundedOutput = constrain(unboundedOutput, 0, 1023);
  scaledOutput = map(boundedOutput, 0, 1023, 0, 255);

  OnOff(tempSensorValue);

  if (DEBUG) {
    Serial.print("Temp sensor value = ");
    Serial.println(tempSensorValue);
    Serial.print("LDR sensor value = ");
    Serial.println(ldrSensorValue);
    Serial.print("setPoint = ");
    Serial.println(setPoint);
    Serial.print("scaledOutput = ");
    Serial.println(scaledOutput);
  }

  if (ldrSensorValue < LDR_THRESHOLD) {     //If the light is on, activate the system
    digitalWrite(ledPin, HIGH);
    if (usePID) {
      analogWrite(PWMPinA, scaledOutput);   //Set the motor speed to the PID output value
    }
    else {
      analogWrite(PWMPinA, onOffOutput);   //Set the motor speed to the ON OFF output value
    }
  }
  else {                                    //If the light is off, disable the system
    digitalWrite(ledPin, LOW);
    analogWrite(PWMPinA, 0);                //Set the motor speed to zero (stop)
  }

  delay(5);
}

void OnOff(int x)
{
  if (x > (setPoint - 5)) {
    onOffOutput =  0;
  }
  if (x < (setPoint + 5)) {
    onOffOutput =  255;
  }
}

double PID(int x)
{
  double pidOutput = 0;

  //calculate dt
  unsigned long now = millis();
  double dt = (double)(now - prevTime);

  double e0 = setPoint - x;

  //calculate P component
  P = Kp * e0;

  //calculate I component
  I += Ki * (e0 * dt);
  if (I > 1023) {
    I = 1023;
  }
  if (I < 0) {
    I = 0;
  }

  //calculate D component
  D = Kd * ((e0 - e1) / dt);

  //calculate PID Output
  pidOutput = P + I + D;

  e1 = e0;
  prevTime = now;

  return pidOutput;
}

void getNewSerialData() {
  if (Serial.available() > 0) {
    char OpCode = Serial.read();
    String serialData = Serial.readStringUntil('\n');
    serialData.trim();
    if (OpCode == 'P') {
      int duration = serialData.toInt();
      if (duration > 0) {
        Serial.println("***************************");
        Serial.print("Purging the system for ");
        Serial.print(duration);
        Serial.println(" seconds");
        Serial.println("***************************");
        analogWrite(PWMPinA, 255);
        delay(duration * 1000);
      }
    }
    if (OpCode == 'S') {
      int newSP = serialData.toInt();
      Serial.println("***************************");
      Serial.print("The new set point is: ");
      Serial.println(newSP);
      Serial.println("***************************");
      setPoint = (double)newSP;
    }
    if (OpCode == 'A') {
      if (serialData == "PI") {
        Serial.println("***************************");
        Serial.println("Switching to PI control algorithm");
        Serial.println("***************************");
        usePID = 1;
      }
      if (serialData == "ONOFF") {
        Serial.println("***************************");
        Serial.println("Switching to ON OFF control algorithm");
        Serial.println("***************************");
        usePID = 0;
      }
    }
    if (OpCode == 'H') {
      Serial.println("***************************");
      Serial.println("Laboratory room temperature controller\n");
      Serial.print("Current set-point = ");
      Serial.println(setPoint);
      Serial.println("Operation codes:");
      Serial.println("S = change the set-point. I.e: S456 changes the set-point to 456");
      Serial.println("P = activate purge for a duration of time. I.e: P10 activates purge for 10 seconds");
      Serial.println("A = Change control algorithm. PI = Proportional Integral control, ONOFF = On Off control.");
      Serial.println("***************************");
    }
  }
}